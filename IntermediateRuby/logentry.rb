#!/usr/bin/ruby

LogEntry class:
	def LogEntry
 		attr_accessor :ip_address, :time_stamp,:request, :response_code, :file_size, :http_referer, :user_agent

 		def initialize row = nil
	 		if row
				row.gsub! /\t/, "  "
				match_data = parse_row row
				set_properties match_data
			end
		end
	
		def set_properties match_data
			@ip_addres = match_data[1]
			@request = match_data[10]
			@response_code = match_data[11]
			@file_size = match_data[12]
			@http_referer = match_data[13]
			@user_agent = match_data[14]
		end
	end









# function that parse log that contains
# 10.0.1.144 -- [03/Jan/2015:02:20:23 +0000] "GET /maintainance/ 
# index/timeout HTTP/1.1" 200 623 "-" "Wget/1.13.4(linux-gnu)"
def parse_row row
	regex = /(\d{1,3}\.\d{1,3}\.\d{1,3})(\S*)(\S*)\[(\d\d)\/([^\/]*)\/(\d{4}):(\d\d):(\d\d):(\d\d)[\+-]\d{4}\]"([^"]*)" (\d+)(\d+)"([^"]*)"/

	regex.match row
end


