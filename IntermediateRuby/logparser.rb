#!/usr/bin/ruby

#@controller = LogParserController

@controller = LogParserController.new
@controller.run

class LogFile
	def cd path
		if Dir.exist?(path)
			@file_path = path
			@directory = Dir.new(@file_path)
			@directory_index = 0
			@list_start = 0
			true
		else
			false
		end
	end
end
