# Introduction to Ruby Programming Language On Linux

This course will teach the basics of the Ruby programming language. We will cover basics of installing Ruby via the package manager of CentOS and Ubuntu, as well as installing Ruby with RVM, the Ruby Version Manager. We will learn the basic syntax of Ruby, and cover how to code our own methods, classes, modules and much more. Through the Intermediate and Advanced sections of the course we will build an Apache log parsing program. We will build upon it throughout the later lessons, until we end up with a nice working log parsing program.

Course Content

###### Learning Basic Ruby Syntax
###### Start Building a Log Parsing Program
###### Dive into concurrent processing models.