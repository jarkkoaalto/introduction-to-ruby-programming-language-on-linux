#!/usr/bin/ruby
# Thread with stop and run
# What state is my thread?
# .status
# "sleep" => the thread is sleep
# "run" => the thread is current running
# "aborting" => the thread has been signaled to exit
# false => the thread has existed normally
# nil => the thread has existed abnormally

my_thread = Thread.new do
	while true do
		puts "thread here"
		Thread.stop
	end
end

time = 0

while time < 30 do
	puts "main thread here"
	my_thread.run
	sleep 1
	time += 1
end
