#!/usr/bin/ruby
#
	def file_dialog_select
  	   load_response = Queue.new
	   load_thread = Thread.new do
		begin
			#Store response on queue
			load_response << @log_file.select_directory_or_load_file		
		rescue NotAnApacgeAccessLog
			@log_file.clear_file
			@current_view.notice "File does not conform to Access Log pattern"
		rescue NoFileAccess, NoDirAccess
			@log_file.clear_file
			@current_view.notice "File or Direcotry Access Not Permitted"
		end
	
	   display_thread = Thread.new do
			while load_thread.status != nil && load_thread.status != false do
				if @log_file.file_inirialized?
					if @log.file.file_percent_loaded != 1
						@current_view.progess_bar @log_file.file_percet_loaded, "File Loading"
					else
						@current_view.progress_bar @log_file.parse_percent, "File Parsing"
					end
				end
			Thread.pass
		end
	end

	def apply_sort_filter
		load_thread = Thread.new do
			@log_file.log_entries = []
			@log_file.clear_file
			@log_file.select_directory_or_load_file
	end
end				
