#!/usr/bin/ruby
#
class LogFile
attr_accessor : file_name, :file_path, :log_entries,
	:directory, :directory_index, :log_entry_index, :list_start,
	:sort_filter, :parse_percent

	def initialize
		cd "./"
		@log_entries = Arraw.new
		@sort_filter = SortFilter.new
		@parse_parcent = 0.0
		@actual_file = nil
	end

	def file_inintialized?
		@actual_file != nil
	end
	
	def file_pecent_loaded
		@actual_file.pos.to_f / @actual_file.size.to_f
	end

	def clear_file
		@actual_file = nil
	end

	def load_file
		begin
			if File.file?(@file_path + @directory.entries[@directory_index])
				@file_name = @directory.entries[@directory_index]
				@actual_file = File.new(@file_path + @file_name)
				@file_name = @directory.entries[@directory_index]
				log_array = @actual_file.readlines
				@parse_percent = 0.0
				log_array.each_with_index do |log, index|
					@log_entries[index] = LogEntry.new log
					@parse_percent = index.to_f / log_array.count.to_f
			end
			@log_entry_index = 0
			@list_start = 0
			true
			else
			
end
