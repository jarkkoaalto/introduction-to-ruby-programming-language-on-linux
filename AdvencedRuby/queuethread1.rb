#!/usr/bin/ruby
# Remarking our ticktock program with queue
require = 'thread'
my_queue = Queue.new
my_var = ""
my_thread = Thread.new do
	10.times do
		my_queue << "tock" # << same as puts
	end
end

10.times do
	my_var += "tick"
	my_var += my_queue.pop
	puts "Value: \t#{my_var}"
end
