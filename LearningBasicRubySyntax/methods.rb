#!/usr/bin/ruby
# methods perform a collection of action

def hello_user
	puts "Enter your name:"
	username = gets
	puts "Hello " + username
	# "Hello #{username.chop}." // if you don't wanna return nil
end

hello_user

