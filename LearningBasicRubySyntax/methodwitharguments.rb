#!/usr/bin/ruby
#
# example methods with arguments
#
def double_my_number number
	number *= 2
	puts number
end

double_my_number 200

puts "=================="

def d_m_n number = 42
	number *=2
end

d_m_n

puts "=================="

def my_method
	puts "executing your code ..."
	yield
	puts "done"
end

my_method do
	puts 2+2
end

