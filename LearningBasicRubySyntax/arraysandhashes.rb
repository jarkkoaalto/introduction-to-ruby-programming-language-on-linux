#!/src/bin/ruby

my_array = [1,2,3,4,5,6,7,8,9,10]

odd_or_even = my_array.map do |element| 
	element % 2 == 0 ? "even" : "odd"
end
puts odd_or_even
