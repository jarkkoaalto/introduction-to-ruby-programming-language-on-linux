#!/usr/bin/ruby
#
#Variable examples
#variable scope
#
#1) local, begin with a lowerscale letter
#2) instance: begin with@
#3) class: begin with @@
#4) golbal. begin with $
#
#
class Square

	def initialize
		@lenght = 10
	end
	
	def printout
		puts "Length = #{@length}."
	end
end

# Variables in classes: local variables. Accessed only within the methods that define them

class Square1
	def initialize
		@length = 10
	end
	def printout
		length = @length * 2
		puts "Length = #{length}."
	end
end

# variables in classes: classes variables, accessed by all instance and methods

class Square2
	@@number_of_squares = 0
	def initialize
		@length = 10
		@@number_of_squares +=1
	end
	def printout
		puts "Length = #{@length}."
		puts "Total = #{@@number_of_squares}."
	end
end

# global variables: can be accessed anywhere
#
$settings = {:border_width => 1}

class Square3
	def initialize length = 10
	@length = length
	@border_width = $settings[:border_width]
	end
end
