#!/src/bin/ruby
# try it yourself
# Write a program that asks the user whetger they need a specific grocery item or not,
# the print out the grocery list at the end of the program.
# Use iterators so that code is not repeated unnecessarily.
#
grocery_item = {"oranges" => false, "bananas" => false, "apples" => false}
puts "Do You Need:"
grocery_item.each do | item, need_for_item|
	puts item + "? (y/n)"
	case gets
		when "y\n"
			grocery_item[item] = true
		when "n\n"
			grocery_item[item] = false
	end
end

puts "Here's Your List:"
grocery_item.each do | item, need_for_item|
	puts item if need_for_item
end
