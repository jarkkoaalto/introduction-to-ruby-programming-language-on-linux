#!/usr/bin/ruby
#
# Try it youself:
# Write a program that works like an adding machine. As long as the user
# types in more numbers, continue to add them up.
# If the user types in a empty line, exit the program and give the total.

total = 0
puts "Imput your numbers"
while input = gets do
	if input == "\n"
		break
	end
	total = total + input.to_f
	puts "running total = #{total}"
end
puts "Total: #{total}"
