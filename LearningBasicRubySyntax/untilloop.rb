#!/usr/bin/ruby
#  until loop is inverse of while loop

my_var = 10
until my_var > 30 do
	puts "my_var = #{my_var}"
	my_var += 1
end
