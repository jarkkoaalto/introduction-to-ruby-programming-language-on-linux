#!/usr/bin/ruby
# tell ruby what to do when you instanstiate a class
#
class Table
	def initialize ilength = 4, iwidth = 3, iheight = 3
		@lenghth = ilength
		@width = iwidth
		@height = ihright
	end
end

# can use 
# my_table = Table.new
# or
# my_table = Table.new 5, 10, 3
#
# Private methods can only be accessed with the class code, not outside of it
#class Table
#	private
#	def table_top_area
#		@length * @width
#	end
#end
# can't call HUOM!!!  Table.table_top_area
