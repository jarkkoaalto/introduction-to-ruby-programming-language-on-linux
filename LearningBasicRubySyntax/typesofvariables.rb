#!/usr/bin/ruby
# Different variable types:
#
# Common types:

my_integer = 0
puts(my_integer);
my_float = 1.0634535
puts(my_float);

light_on = true
puts(light_on);

ridiculus_test = "The quick brown fos jumped"
silly_string = 'ove the lazy dog'
puts(ridiculus_test);
puts(silly_string);

first_four_numbers = [1,2,3]


puts(first_four_numbers[1]);

my_array  = [] # Empty array

my_hash = {"simple string" => "my String", "my number" => 4}

puts(my_hash["simple string"])

my_favorites_array = [9,"blue"];
puts(my_favorites_array[0]);
puts(my_favorites_array[1]);
