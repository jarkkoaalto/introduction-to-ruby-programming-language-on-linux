#!/usr/bin/ruby
#Try it yourself:
#Write a program that asks the user for 10 integers. then sort the
#integers from least to greatest. Output the sorted lisy to the user

integers = []
current_integer = 0

while current_integer < 10 do
	puts "Type an integer"
	integers[current_integer] = gets.to_i
	current_integer += 1
end

integers.sort.each do |this_int| 
	puts this_int
end
