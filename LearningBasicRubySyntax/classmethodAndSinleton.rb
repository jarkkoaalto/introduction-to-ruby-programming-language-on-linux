#!/usr/bin/ruby
#
#define a class method
#
class Tree
	def self.trim
		"All trees are trimmed now!"
	end
end

# call this Tree.trim
#
# Singleton method. Amethod that is defined on only one object

abc = "abc"

def abc.twice
	"#{self}#{self}"
end

# call this abc.twice

