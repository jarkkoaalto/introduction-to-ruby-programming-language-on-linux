#!/usr/bin/ruby
# Using math in program
#
a = 6*5
puts(a);
base = 4
height = 4
area = base * height
puts("Area is ", area);

length = 10
height = 20
width = 50
volume = length * height * width
puts(volume);

puts(100.to_s);
puts("100".to_i);
puts("20".to_f);
