#!/usr/bin/ruby
#
# simple while loop example

my_var = 0


while my_var<10 do

	puts "my_var = #{my_var}"
	my_var += 1
end
