#!/usr/bin/ruby
#
# create file. Inside file define an array consisting of the numbers 1-3
# define another arrray consisting of the numbers written out as string
# instruct the user to type in either 1 or one
# use gets to get the user input
# if the user typed 1 output the three elements the the first array. If the user
# typed one then output the elements of the second array.
# run your program to test it.
#
array123 = [1,2,3]
array_string123 = ["one", "two","three"]
puts "Type 1 for integer, or 'one' for strings"
user_input = gets.strip

if user_input == "1"
	puts "#{array123[0]},#{array123[1]},#{array123[2]}"
elsif user_input == "one"
	puts "#{array_string123[0]},#{array_string123[1]},#{array_string123[2]}"
end
