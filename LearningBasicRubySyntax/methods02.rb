#!/usr/bin/ruby
# Methods sometimes return boolean
#
def hello_user
	puts "Enter your name"
	username = gets
	if username != "\n"
		"hello #{username.chop}." 
	else
		false
	end
end

hello_user
