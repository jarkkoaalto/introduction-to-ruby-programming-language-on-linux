#!/usr/bin/ruby
#
#Try it yourself
#Write a class for a shippling company. This class will correspond to a 
#box that will be shipped. The class should include:
#	class variables for material cost
#	class methods for changing the cost of materials
#	instance variable for size, weight, and travel distance
#	a method for calculating the cost of the individual package
#
class Boxing
	attr_accessor:length, :width, :height, :weight, :distance
	def initialize
		@@material_cost = 0.01
		@@rate = 0.01
	end
	
	def self.rate= rate
		@@rate = rate
	end

	def self.materials_cost= cost
		@@materials_cost = cost
	end
	
	def package_cost
		(@length * @wigth * 2 + @length * @hright * 2 + @width * height * 2) * @@matrials_cost + @weight * @distance * @@rate
	end
end

