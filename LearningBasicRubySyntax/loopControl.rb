#!/usr/bin/ruby
# differrent examples of loops

my_var = 0
while my_var < 10 do
	if my_var == 3
		my_var +=1
		next
	end
	puts my_var
	my_var += 1
end

puts "======="
## redo key-word
# Goes to the beginnig of this iteration of the loop, whether or not
# the condition is still true
my_var1 = 0;
while my_var1 < 10 do
	puts my_var1
	if my_var1 == 3
		my_var1 = 10
		redo
	end
	my_var1 += 1
end

puts "======="

# break: exit the loop immediately

puts "type something to continue. Or nothing to quit"

while a = gets do
	if a == "\n"
		break
	end
	puts a
end
