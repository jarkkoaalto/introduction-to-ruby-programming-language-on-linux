#!/usr/bin/ruby
# simple for loop example

bags = ["suitcase", "messanger bag","satchel","backpack"]

for bag_type in bags do
	puts bag_type
end
