#!/src/bin/ruby
# If else example
#

my_variable = 0;

if my_variable < 10
	puts my_variable;
else
	puts "Too Small!";
end

# tenary statement:

if my_variable > 10
	puts "big"
else 
	puts "small"
end

#or

my_variable > 10 ? puts("big") : puts("small")
