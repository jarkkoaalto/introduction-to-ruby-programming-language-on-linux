#!/usr/bin/ruby
#
# Exercise: Writing methods
# 1. Write a method, area, which takes one argument, radius
# 2. Have the method return the area of circle with the given radius
# 3. Use this method in a program witch prompts the user for a radius and displays the result of the method given this radius
# 4. test the user input. If it negative, inform the user of the error instead of calculating
# 5. Wxapnd the program to calculate and output the circumstance circle too.
#

puts "Please input a radius to calculate the area and circumference of a circle."
input = gets.to_f
def area radius
	Math::PI * radius ** 2
end

def circumference radius
	2 * Math::PI * radius
end

if input >= 0
	puts "Area = #{area input}"
	puts "Circumferenc =#{circumference input}"
else
	puts "Radius #{input} is invalid"
end
